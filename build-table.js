const store = {
  page: 1,
  isLoaded: false,
};

const table = document.querySelector("table");
var select = document.getElementById("selectNumber");

function generateTableHead(table, data) {
  let thead = table.createTHead();
  let row = thead.insertRow();
  for (let key of data) {
    let th = document.createElement("th");
    let text = document.createTextNode(key);
    th.appendChild(text);
    row.appendChild(th);
  }
}

function generateTable(table, data) {
  for (let element of data) {
    let row = table.insertRow();
    for (key in element) {
      let cell = row.insertCell();
      let text = document.createTextNode(element[key]);
      cell.appendChild(text);
    }
  }
}

const advanced = (page) => {
  return page > 1;
};

const runFetch = (pageNum) => {
  console.log(pageNum, "num is");
  advanced(pageNum)
    ? (document.getElementById("Prev").className = "show")
    : (document.getElementById("Prev").className = "hide");
  console.log(advanced(pageNum), "advanced");
  fetch(`https://jsonplaceholder.typicode.com/todos?_page=${pageNum}&_limit=3`)
    .then(function (response) {
      // The API call was successful!
      if (response.ok) {
        store.isLoaded = true;
        return response.json();
      } else {
        return Promise.reject(response);
      }
    })
    .then(function (mountains) {
      // This is the JSON from our response
      console.log(mountains);

      // let table = document.querySelector("table");

      const setTables = () => {
        table.innerHTML = "";
        let data = Object.keys(mountains[0]);
        generateTableHead(table, data);
        generateTable(table, mountains);
      };

      return checkIsLoaded ? setTables() : (table.innerHTML = "loading...");
    })
    .catch(function (err) {
      // There was an error
      console.warn("Something went wrong.", err);
    });
};

document.getElementById("showSelect").onclick = function () {
  console.log("select box was clicked");
  const Form = (document.getElementById("myForm").className = "show");
  table.innerHTML = "";
  document.getElementById("table-div").className = "hide";
  var options = ["Mazda", "Subaru", "Jeep", "Camry", "Toyota"];
  // Optional: Clear all existing options first:
  select.innerHTML = "";
  // Populate list with options:
  for (var i = 0; i < options.length; i++) {
    var opt = options[i];
    select.innerHTML += '<option value="' + opt + '">' + opt + "</option>";
  }
};

document.getElementById("Next").onclick = function () {
  console.log("next was clicked");
  table.innerHTML = "loading...";
  store.page++;
  runFetch(store.page);
};

document.getElementById("Prev").onclick = function () {
  console.log("next was clicked");
  table.innerHTML = "loading...";
  store.page--;
  runFetch(store.page);
  console.log(store, "store");
};

const checkIsLoaded = () => {
  const loaded = state.loaded;
  if (loaded) {
    return true;
  }
  return false;
};

function fnExcelReport() {
  var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
  var textRange;
  var j = 0;
  tab = document.getElementById("table"); // id of table

  for (j = 0; j < tab.rows.length; j++) {
    tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
    //tab_text=tab_text+"</tr>";
  }

  tab_text = tab_text + "</table>";
  tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, ""); //remove if u want links in your table
  tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
  tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

  var ua = window.navigator.userAgent;
  var msie = ua.indexOf("MSIE ");

  if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
    // If Internet Explorer
    txtArea1.document.open("txt/html", "replace");
    txtArea1.document.write(tab_text);
    txtArea1.document.close();
    txtArea1.focus();
    sa = txtArea1.document.execCommand(
      "SaveAs",
      true,
      "Say Thanks to Sumit.xls"
    );
  } //other browser not tested on IE 11
  else
    sa = window.open(
      "data:application/vnd.ms-excel," + encodeURIComponent(tab_text)
    );

  return sa;
}
window.onload = function () {
  table.innerHTML = "loading...";
  runFetch(store.page);
};
